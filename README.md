Philippe PB


Philippe Pulwermacher-Blanchard est un musicien folk / écrivain qui s’étale sur divers domaines tel que la politique ou encore la poésie. Dans un but de centraliser ses productions et ainsi éviter de se disperser sur diverses plateformes, il souhaite mettre en place un site internet.
La charte graphique :
Le client n’a pas vraiment de préférence graphique. Cependant une interface rappelant les inspirations de Philippe (artistes engagés, musique Folk, etc...)  mais sobre serait le bienvenue.
Comme artistes engagés, on peut citer Bob Dylan ainsi que le chanteur français Renaud dans sa jeunesse.
Des teintes rouge / marron / jaune pâle sont un point de départ, mais les développeurs sont libres de partir vers de nouvelles idées.

Le site sera responsive pour les gens qui voudraient écouter Philippe sur leur téléphone.
Les fonctionnalités principales :
Le front office
Le contenu statique :

Le site est découpé en 6 sections :
Les albums
Les vidéos
Les écrits
Les articles
Les photos
Les liens utiles et Contact
 
La page d’accueil
Sur la page d’accueil, on souhaite retrouver les derniers contenu produit par Philippe, soit les sections décrites ci-dessus.
 
Dans chacune de ces sections, l’utilisateur aura le choix parmi plusieurs catégories (piano, guitare folk, mix, poésie, autres)
Ne pas oublier de prendre en compte qu’une musique peut-être composée d’une guitare et d’un piano, il faut donc pouvoir la ranger dans ces 2 catégories.
Ces catégories s’appliquent à chaque section, on peut trouver de la poésie dans les vidéos.
Les articles ne sont que des messages de Philippe adressés aux utilisateurs du site.
Le contenu
Un chapeau court (modifiable dans le back office) qui décrit l’artiste.
Les derniers articles postés doivent apparaître en-dessous.
Dans la section liens utiles se trouvera une formulaire de Contact.
Les sections & catégories

Si l’utilisateur clique sur une catégorie, il retrouve la liste des articles correspondant à cette catégorie dans cette section.

S’il a cliqué sur une section, alors il n’y aura pas de filtre sur les catégories.

Dans la section album il est nécessaire d’avoir les informations suivantes :
Pochette.
Titre.
Description.
Durée du titre.

Lorsqu’on clique sur un album, une modal apparaît avec la listes des titres de l’album. Cliquer sur un des titres lance la chanson. L'apparition des paroles serait un plus.

Attention pour les vidéos, elle peuvent être hébergées ou tirées de youtube.

Pour les autres articles, un simple affichage par ordre chronologique sera suffisant.
               
Le back office

Graphiquement, le back office est très épuré.
Vous pouvez vous baser sur celui-ci : https://blackrockdigital.github.io/startbootstrap-sb-admin-2/pages/index.html

Dans ce back office, l’administrateur doit pouvoir :
CRUD (Create, Read, Update, Delete) des catégories.
CRUD des albums.
Pouvoir ajouter des titres à un ou plusieurs albums.
CRUD des titres.
CRUD des articles / photos / écrits.
Mettre à jour ses liens utiles et ses contacts.
CRUD des vidéos avec choix d’upload la vidéo ou de mettre un lien youtube
Les sections fixes, nul besoin d’avoir la main dessus.
Les fonctionnalités facultatives :

Mettre en place une “radio Philippe” qui diffusera aléatoirement les titres présents sur le site.

Cette radio proposera de se lancer automatiquement ou non à chaque connexion sur le site lors de la première connexion de l’utilisateur.
Elle s’arrête lorsqu’une vidéo ou une chanson est lancée.
L’utilisateur peut aussi créer sa playlist.
Un espace commentaire pour chaque titre / album / vidéos / article.
Des statistiques d’écoutes.

