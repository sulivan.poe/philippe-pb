<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Commentaire;
use AppBundle\Entity\Ecrit;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Ecrit controller.
 *
 * @Route("/")
 */
class EcritController extends Controller
{
    /**
     * Front ecrit
     *
     * @Route("ecrit/", name="ecrit_front")
     * @Method("GET")
     */
    public function UserAction()
    {
        $em = $this->getDoctrine()->getManager();

        $ecrits = $em->getRepository('AppBundle:Ecrit')->findAll();

        return $this->render('ecrit/front.html.twig', array(
            'ecrits' => $ecrits,
        ));
    }

    /**
     * Finds and displays a ecrit entity.
     *
     * @Route("ecrit/{id}", name="front_ecrit_show")
     */
    public function frontShowAction(Ecrit $ecrit,Request $request)
    {
        $commentaires = $this->getDoctrine()->getRepository('AppBundle:Commentaire')->findBy(['object'=>$ecrit->getId(),'typeObject'=> Commentaire::TYPE_ECRIT]);
        $commentaire = new Commentaire();
        $form = $this->createForm('AppBundle\Form\CommentaireUserType', $commentaire);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $commentaire->setObject($ecrit->getId());
            $commentaire->setTypeObject(Commentaire::TYPE_ECRIT);
            $em->persist($commentaire);
            $em->flush();
            return $this->redirectToRoute('front_ecrit_show',['id'=> $ecrit->getId()]);
        }


        return $this->render(':ecrit:front-show.html.twig', array(
            'ecrit' => $ecrit,
            'form' => $form->createView(),
            'commentaires' => $commentaires
        ));
    }

    /**
     * Lists all ecrit entities.
     *
     * @Route("admin/ecrit/", name="ecrit_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $ecrits = $em->getRepository('AppBundle:Ecrit')->findAll();

        return $this->render('ecrit/index.html.twig', array(
            'ecrits' => $ecrits,
        ));
    }

    /**
     * Creates a new ecrit entity.
     *
     * @Route("admin/ecrit/new", name="ecrit_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $ecrit = new Ecrit();
        $form = $this->createForm('AppBundle\Form\EcritType', $ecrit);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($ecrit);
            $em->flush();

            return $this->redirectToRoute('ecrit_show', array('id' => $ecrit->getId()));
        }

        return $this->render('ecrit/new.html.twig', array(
            'ecrit' => $ecrit,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a ecrit entity.
     *
     * @Route("admin/ecrit/{id}", name="ecrit_show")
     * @Method("GET")
     */
    public function showAction(Ecrit $ecrit)
    {
        $deleteForm = $this->createDeleteForm($ecrit);

        return $this->render('ecrit/show.html.twig', array(
            'ecrit' => $ecrit,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing ecrit entity.
     *
     * @Route("admin/ecrit/{id}/edit", name="ecrit_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Ecrit $ecrit)
    {
        $deleteForm = $this->createDeleteForm($ecrit);
        $editForm = $this->createForm('AppBundle\Form\EcritType', $ecrit);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('ecrit_edit', array('id' => $ecrit->getId()));
        }

        return $this->render('ecrit/edit.html.twig', array(
            'ecrit' => $ecrit,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a ecrit entity.
     *
     * @Route("/admin/ecrit/{id}", name="ecrit_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Ecrit $ecrit)
    {
        $form = $this->createDeleteForm($ecrit);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($ecrit);
            $em->flush();
        }

        return $this->redirectToRoute('ecrit_index');
    }

    /**
     * Creates a form to delete a ecrit entity.
     *
     * @param Ecrit $ecrit The ecrit entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Ecrit $ecrit)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('ecrit_delete', array('id' => $ecrit->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
