<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Commentaire;
use AppBundle\Entity\Photos;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Photo controller.
 *
 * @Route("/")
 */
class PhotosController extends Controller
{
    /**
     * Front photo
     *
     * @Route("photo/", name="photo_front")
     * @Method("GET")
     */
    public function UserAction()
    {
        $em = $this->getDoctrine()->getManager();

        $photos = $em->getRepository('AppBundle:Photos')->findAll();

        return $this->render('photos/front.html.twig', array(
            'photos' => $photos,
        ));
    }

    /**
     * Finds and displays a photo entity.
     *
     * @Route("photo/{id}", name="front_photo_show")
     */
    public function frontShowAction(Photos $photo,Request $request)
    {
        $commentaires = $this->getDoctrine()->getRepository('AppBundle:Commentaire')->findBy(['object'=>$photo->getId(),'typeObject'=> Commentaire::TYPE_PHOTO]);
        $commentaire = new Commentaire();
        $form = $this->createForm('AppBundle\Form\CommentaireUserType', $commentaire);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $commentaire->setObject($photo->getId());
            $commentaire->setTypeObject(Commentaire::TYPE_PHOTO);
            $em->persist($commentaire);
            $em->flush();
            return $this->redirectToRoute('front_photo_show',['id'=> $photo->getId()]);
        }


        return $this->render('photos/front-show.html.twig', array(
            'photo' => $photo,
            'form' => $form->createView(),
            'commentaires' => $commentaires
        ));
    }
    /**
     * Lists all photo entities.
     *
     * @Route("admin/photo/", name="photos_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $photos = $em->getRepository('AppBundle:Photos')->findAll();

        return $this->render('photos/index.html.twig', array(
            'photos' => $photos,
        ));
    }

    /**
     * Creates a new photo entity.
     *
     * @Route("admin/photos/new", name="photos_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $photo = new Photos();
        $form = $this->createForm('AppBundle\Form\PhotosType', $photo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($photo);
            $em->flush();

            return $this->redirectToRoute('photos_show', array('id' => $photo->getId()));
        }

        return $this->render('photos/new.html.twig', array(
            'photo' => $photo,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a photo entity.
     *
     * @Route("admin/photos/{id}", name="photos_show")
     * @Method("GET")
     */
    public function showAction(Photos $photo)
    {
        $deleteForm = $this->createDeleteForm($photo);

        return $this->render('photos/show.html.twig', array(
            'photo' => $photo,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing photo entity.
     *
     * @Route("admin/photos/{id}/edit", name="photos_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Photos $photo)
    {
        $deleteForm = $this->createDeleteForm($photo);
        $editForm = $this->createForm('AppBundle\Form\PhotosType', $photo);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('photos_show', array('id' => $photo->getId()));
        }

        return $this->render('photos/edit.html.twig', array(
            'photo' => $photo,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a photo entity.
     *
     * @Route("admin/photos/{id}", name="photos_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Photos $photo)
    {
        $form = $this->createDeleteForm($photo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($photo);
            $em->flush();
        }

        return $this->redirectToRoute('photos_index');
    }

    /**
     * Creates a form to delete a photo entity.
     *
     * @param Photos $photo The photo entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Photos $photo)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('photos_delete', array('id' => $photo->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
