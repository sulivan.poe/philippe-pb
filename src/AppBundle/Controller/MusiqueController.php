<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Commentaire;
use AppBundle\Entity\Musique;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Musique controller.
 *
 * @Route("/")
 */
class MusiqueController extends Controller
{
    /**
     * Front musique
     *
     * @Route("musique/", name="musique_front")
     * @Method("GET")
     */
    public function UserAction()
    {
        $em = $this->getDoctrine()->getManager();

        $musiques = $em->getRepository('AppBundle:Musique')->findAll();

        return $this->render('musique/front.html.twig', array(
            'musiques' => $musiques,
        ));
    }

    /**
     * Finds and displays a musique entity.
     *
     * @Route("musique/{id}", name="front_musique_show")
     */
    public function frontShowAction(Musique $musique,Request $request)
    {
        $commentaires = $this->getDoctrine()->getRepository('AppBundle:Commentaire')->findBy(['object'=>$musique->getId(),'typeObject'=> Commentaire::TYPE_MUSIQUE]);
        $commentaire = new Commentaire();
        $form = $this->createForm('AppBundle\Form\CommentaireUserType', $commentaire);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $commentaire->setObject($musique->getId());
            $commentaire->setTypeObject(Commentaire::TYPE_MUSIQUE);
            $em->persist($commentaire);
            $em->flush();
            return $this->redirectToRoute('front_musique_show',['id'=> $musique->getId()]);
        }


        return $this->render('musique/front-show.html.twig', array(
            'musique' => $musique,
            'form' => $form->createView(),
            'commentaires' => $commentaires
        ));
    }
    /**
     * Lists all musique entities.
     *
     * @Route("admin/musique/", name="musique_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $musiques = $em->getRepository('AppBundle:Musique')->findAll();

        return $this->render('musique/index.html.twig', array(
            'musiques' => $musiques,
        ));
    }

    /**
     * Creates a new musique entity.
     *
     * @Route("admin/musique/new", name="musique_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $musique = new Musique();
        $form = $this->createForm('AppBundle\Form\MusiqueType', $musique);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($musique);
            $em->flush();

            return $this->redirectToRoute('musique_show', array('id' => $musique->getId()));
        }

        return $this->render('musique/new.html.twig', array(
            'musique' => $musique,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a musique entity.
     *
     * @Route("admin/musique/{id}", name="musique_show")
     * @Method("GET")
     */
    public function showAction(Musique $musique)
    {
        $deleteForm = $this->createDeleteForm($musique);

        return $this->render('musique/show.html.twig', array(
            'musique' => $musique,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing musique entity.
     *
     * @Route("admin/musique/{id}/edit", name="musique_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Musique $musique)
    {
        $deleteForm = $this->createDeleteForm($musique);
        $editForm = $this->createForm('AppBundle\Form\MusiqueType', $musique);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('musique_edit', array('id' => $musique->getId()));
        }

        return $this->render('musique/edit.html.twig', array(
            'musique' => $musique,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a musique entity.
     *
     * @Route("admin/musique/{id}", name="musique_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Musique $musique)
    {
        $form = $this->createDeleteForm($musique);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($musique);
            $em->flush();
        }

        return $this->redirectToRoute('musique_index');
    }

    /**
     * Creates a form to delete a musique entity.
     *
     * @param Musique $musique The musique entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Musique $musique)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('musique_delete', array('id' => $musique->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
